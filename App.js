import React, { useState } from 'react';
import './css/app.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  const [candidatos, setCandidatos] = useState([
    {
      nome: 'John Cena',
      partido: 'Binchillin',
      votos: 0
    },
    {
      nome: 'ChilliP',
      partido: 'Flamengo',
      votos: 0  
    },
    {
      nome: 'pião',
      partido: 'do bau',
      votos: 0  
    }
  ]);

  const [mostrarResultados, setMostrarResultados] = useState(false);

  const incrementarVotos = (index) => {
    const novosCandidatos = [...candidatos];
    novosCandidatos[index].votos++;
    setCandidatos(novosCandidatos);
  }

  const calcularVencedor = () => {
    let maiorNumeroDeVotos = 0;
    let indiceDoVencedor = 0;
    
    for (let i = 0; i < candidatos.length; i++) {
      if (candidatos[i].votos > maiorNumeroDeVotos) {
        maiorNumeroDeVotos = candidatos[i].votos;
        indiceDoVencedor = i;
      }
    }
    
    return candidatos[indiceDoVencedor];
  }

  const zerarVotos = () => {
    const novosCandidatos = candidatos.map((candidato) => {
      return {
        ...candidato,
        votos: 0
      }
    });
    setCandidatos(novosCandidatos);
    setMostrarResultados(false);
  }

  if (mostrarResultados) {
    const vencedor = calcularVencedor();
    return (
      <div className="container">
        <h1>Resultado da Eleição</h1>
        <p>O vencedor foi {vencedor.nome} do partido {vencedor.partido} com {vencedor.votos} votos!</p>
        <button className="button-resultado" onClick={zerarVotos}>STOP THE COUNT!!!</button>
      </div>
    );
  } else {
    return (
      <div className="container">
        <h1>Eleição para Presidente</h1>
        {candidatos.map((candidato, index) => {
          return (
            <div key={index} className="card">
              <div className="card-body">
                <h5 className="card-title">{candidato.nome}</h5>
                <p className="card-text">Partido: {candidato.partido}</p>
                <p className="card-text">Votos: {candidato.votos}</p>
                <button className="button-votar" onClick={() => incrementarVotos(index)}>Votar</button>
              </div>
            </div>
          );
        })}
        <button className="button-resultado" onClick={() => setMostrarResultados(true)}>Mostrar Resultados</button>
      </div>
    );
  }
}

export default App;
